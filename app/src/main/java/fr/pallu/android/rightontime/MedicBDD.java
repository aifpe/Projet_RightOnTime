package fr.pallu.android.rightontime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class MedicBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "medic.db";
    private static final String TABLE_MEDIC = "table_medic";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NOMMEDIC = "NOMMEDIC";
    private static final int NUM_COL_NOMMEDIC = 1;
    private static final String COL_TRAITEMENT = "TRAITEMENT";
    private static final int NUM_COL_TRAITEMENT = 2;
    private static final String COL_EFFETSSECONDAIRES = "EFFETSSECONDAIRES";
    private static final int NUM_COL_EFFETSSECONDAIRES = 3;
    private static final String COL_CONTREINDICATION = "CONTREINDICATION";
    private static final int NUM_COL_CONTREINDICATION = 4;
    private static final String COL_INDICATIONUTILISATION = "INDICATIONUTILISATION";
    private static final int NUM_COL_INDICATIONUTILISATION = 5;
    private static final String COL_MODEADMINISTRATION = "MODEADMINISTRATION";
    private static final int NUM_COL_MODEADMINISTRATION = 6;
    private static final String COL_NBPILULEAPRENDRE = "NBPILULEAPRENDRE";
    private static final int NUM_COL_NBPILULEAPRENDRE = 7;
    private static final String COL_PRIX = "PRIX";
    private static final int NUM_COL_PRIX = 8;
    private static SQLiteDatabase bdd;
    private MaBaseSQLite maBaseSQLite;

    public MedicBDD(Context context){
        maBaseSQLite = new MaBaseSQLite(context,NOM_BDD,null,VERSION_BDD);
    }
    public void open(){
        bdd = maBaseSQLite.getWritableDatabase();
    }
    public void close(){
        bdd.close();
    }
    public void read(){
        bdd = maBaseSQLite.getReadableDatabase();
    }
    public void removeAll(){
        bdd.delete(TABLE_MEDIC,null,null);
    }


    public ArrayList queueAll(){
        String[] column = new String[]{COL_ID +" AS _id",COL_NOMMEDIC};
        Cursor c = bdd.query(TABLE_MEDIC, column, null, null, null, null, COL_NOMMEDIC);
        ArrayList<String> mArrayList = new ArrayList<String>();
        int columnIndex=c.getColumnIndex(maBaseSQLite.COL_NOMMEDIC);
        while(c.moveToNext()) {
            mArrayList.add(c.getString(columnIndex)); //add the item
        }
        c.close();
        return mArrayList;
    }


    public SQLiteDatabase getBdd(){
        return bdd;
    }

    public boolean insertMedic(Medic medic){
        if(!CheckIsDataAlreadyInDBorNot(TABLE_MEDIC,COL_NOMMEDIC,medic.getNomMedic())) {
            ContentValues values = new ContentValues();
            values.put(COL_NOMMEDIC, medic.getNomMedic());
            values.put(COL_TRAITEMENT, medic.getTraitement());
            values.put(COL_EFFETSSECONDAIRES, medic.getEffetsSecondaires());
            values.put(COL_CONTREINDICATION, medic.getContreIndication());
            values.put(COL_INDICATIONUTILISATION, medic.getIndicationUtilisation());
            values.put(COL_MODEADMINISTRATION, medic.getModeAdministration());
            values.put(COL_NBPILULEAPRENDRE, medic.getNbPiluleAPrendre());
            values.put(COL_PRIX, medic.getPrix());
            bdd.insert(TABLE_MEDIC, null, values);
            return true;
        }else
            return false;
    }

    public static boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                                      String dbfield, String fieldValue) {
        SQLiteDatabase sqldb = MedicBDD.bdd;
        String Query = "Select * from " + TableName + " where " + dbfield + " = '" + fieldValue+"';";
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public Medic getMedicByNomMedic(String nom){
        SQLiteDatabase sqldb = MedicBDD.bdd;
        String Query = "Select * from " + TABLE_MEDIC + " where " + COL_NOMMEDIC + " = '" + nom+"'";
        Cursor cursor = sqldb.rawQuery(Query, null);
        return cursorToMedic(cursor);
    }


    public int updateMedic(int id, Medic medic){
        ContentValues values = new ContentValues();
        values.put(COL_NOMMEDIC,medic.getNomMedic());
        values.put(COL_TRAITEMENT,medic.getTraitement());
        values.put(COL_EFFETSSECONDAIRES,medic.getEffetsSecondaires());
        values.put(COL_CONTREINDICATION,medic.getContreIndication());
        values.put(COL_INDICATIONUTILISATION,medic.getIndicationUtilisation());
        values.put(COL_MODEADMINISTRATION,medic.getModeAdministration());
        values.put(COL_NBPILULEAPRENDRE,medic.getNbPiluleAPrendre());
        values.put(COL_PRIX,medic.getPrix());

        return bdd.update(TABLE_MEDIC, values, COL_ID + " = " + id, null);
    }

    public int removeMedicWithNomMedic(String nom) {
        return bdd.delete(TABLE_MEDIC, COL_NOMMEDIC + " = '" + nom+"'", null);
    }

    public Medic cursorToMedic(Cursor c){
        if(c.getCount() == 0)
            return null;

        c.moveToFirst();
        Medic medic = new Medic();
        medic.setId(c.getInt(NUM_COL_ID));
        medic.setNomMedic(c.getString(NUM_COL_NOMMEDIC));
        medic.setTraitement(c.getString(NUM_COL_TRAITEMENT));
        medic.setEffetsSecondaires(c.getString(NUM_COL_EFFETSSECONDAIRES));
        medic.setContreIndication(c.getString(NUM_COL_CONTREINDICATION));
        medic.setIndicationUtilisation(c.getString(NUM_COL_INDICATIONUTILISATION));
        medic.setModeAdministration(c.getString(NUM_COL_MODEADMINISTRATION));
        medic.setNbPiluleAPrendre(c.getString(NUM_COL_NBPILULEAPRENDRE));
        medic.setPrix(c.getInt(NUM_COL_PRIX));
        c.close();

        return medic;
    }

    public String get_COLNOMMEDIC(){
        return "NOMMEDIC";
    }
    public String get_TableMedic(){
        return TABLE_MEDIC;
    }

}
