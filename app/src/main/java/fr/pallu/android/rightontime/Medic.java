package fr.pallu.android.rightontime;


public class Medic {
    private int id;
    private String nomMedic,traitement,effetsSecondaires,contreIndication,indicationUtilisation,modeAdministration,nbPiluleAPrendre;
    private float prix;
    // Description des attributs dans les getters

    public Medic(){
    }

    public Medic(String nomMedic, float prix, String traitement, String effetsSecondaires, String contreIndication, String indicationUtilisation, String modeAdministration, String nbPiluleAPrendre){
        this.prix=prix;
        this.nomMedic=nomMedic;
        this.traitement=traitement;
        this.effetsSecondaires=effetsSecondaires;
        this.contreIndication=contreIndication;
        this.indicationUtilisation=indicationUtilisation;
        this.modeAdministration=modeAdministration;
        this.nbPiluleAPrendre=nbPiluleAPrendre;
    }

    //Identifiant du médicament
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    //Prix du médicament
    public float getPrix() {
        return prix;
    }
    public void setPrix(float prix) {
        this.prix = prix;
    }

    //Nom du médicament
    public String getNomMedic() {
        return nomMedic;
    }
    public void setNomMedic(String nomMedic) {
        this.nomMedic = nomMedic;
    }

    //Utiliser le médicament contre ... (ex: grippe, maux de tête, ...)
    public String getTraitement() {
        return traitement;
    }
    public void setTraitement(String traitement) {
        this.traitement = traitement;
    }

    //Les effets secondaires du médicament
    public String getEffetsSecondaires() {
        return effetsSecondaires;
    }
    public void setEffetsSecondaires(String effetsSecondaires) {
        this.effetsSecondaires = effetsSecondaires;
    }

    //Ne pas prendre le médicament avec un autre
    public String getContreIndication() {
        return contreIndication;
    }
    public void setContreIndication(String contreIndication) {
        this.contreIndication = contreIndication;
    }

    //Utilisable par .. (ex: adulte et enfant de plus de 15 ans)
    public String getIndicationUtilisation() {
        return indicationUtilisation;
    }
    public void setIndicationUtilisation(String indicationUtilisation) {
        this.indicationUtilisation = indicationUtilisation;
    }

    //Comment prendre le médicament
    public String getModeAdministration() {
        return modeAdministration;
    }
    public void setModeAdministration(String modeAdministration) {
        this.modeAdministration = modeAdministration;
    }

    //Nombre de pilule à prendre à chaque prise
    public String getNbPiluleAPrendre() {
        return nbPiluleAPrendre;
    }
    public void setNbPiluleAPrendre(String nbPiluleAPrendre) {
        this.nbPiluleAPrendre = nbPiluleAPrendre;
    }

    public String toString(){
        return "Nom du médicament : "+this.nomMedic+". A utiliser contre : "+this.traitement+".";
    }
}
