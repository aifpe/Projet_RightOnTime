package fr.pallu.android.rightontime;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;


public class Fragment_Delete extends Fragment {
    MedicBDD medicBDD;
    ListView listView;
    Button yes, no;
    TextView nommedic;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delete,container,false);
        medicBDD = new MedicBDD(v.getContext());
        listView = (ListView) v.findViewById(R.id.listview_delete);
        medicBDD.open();

        List<Medic> values = medicBDD.queueAll();

        final ArrayAdapter<Medic> adapter = new ArrayAdapter<Medic>(v.getContext(),
                android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);
        medicBDD.close();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Dialog dialog = new Dialog(view.getContext());
                dialog.setContentView(R.layout.dialog);

                yes = (Button) dialog.findViewById(R.id.dialog_button_yes);
                no = (Button) dialog.findViewById(R.id.dialog_button_no);
                nommedic = (TextView) dialog.findViewById(R.id.dialog_textview_nommedic);

                nommedic.setText(listView.getItemAtPosition(position).toString());

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        medicBDD.open();
                        medicBDD.removeMedicWithNomMedic(nommedic.getText().toString());
                        dialog.dismiss();
                        adapter.notifyDataSetChanged();
                        medicBDD.close();
                        Toast.makeText(v.getContext(), "Le médicament a bien été supprimé", Toast.LENGTH_SHORT).show();
                    }
                });

                dialog.show();
            }
        });

        return v;
    }
}
