package fr.pallu.android.rightontime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class Fragment_Consult extends Fragment {
    TextView nommedic,prix,traitement,effets,contreIndic,quiPeut,modeAdmin,quantite;
    MedicBDD medicBDD;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_consult, container, false);
        medicBDD = new MedicBDD(v.getContext());
        nommedic = (TextView) v.findViewById(R.id.fragment_consult_nommedic);
        prix = (TextView) v.findViewById(R.id.fragment_consult_prix);
        traitement = (TextView) v.findViewById(R.id.fragment_consult_traitement);
        effets = (TextView) v.findViewById(R.id.fragment_consult_effets);
        contreIndic = (TextView) v.findViewById(R.id.fragment_consult_contreIndic);
        quiPeut = (TextView) v.findViewById(R.id.fragment_consult_quiPeut);
        modeAdmin = (TextView) v.findViewById(R.id.fragment_consult_modeAdmin);
        quantite = (TextView) v.findViewById(R.id.fragment_consult_quantite);

        String idConsult = getArguments().getString("idConsult");
        nommedic.setText(idConsult);
        Medic medic = medicBDD.getMedicByNomMedic(idConsult);
        prix.append(" "+Float.toString(medic.getPrix())+" €");
        traitement.append(" " + medic.getTraitement());
        effets.append(" "+medic.getEffetsSecondaires());
        contreIndic.append(" "+medic.getContreIndication());
        quiPeut.append(" "+medic.getIndicationUtilisation());
        modeAdmin.append(" "+medic.getModeAdministration());
        quantite.append(" "+medic.getNbPiluleAPrendre());

        return v;
    }
}
