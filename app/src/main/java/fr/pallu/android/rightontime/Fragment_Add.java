package fr.pallu.android.rightontime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fragment_Add extends Fragment {
    Button reset, addMedic;
    EditText et1, et2, et3, et4, et5, et6, et7, et8;
    MedicBDD medicBDD;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add,container,false);

        reset = (Button) v.findViewById(R.id.buttonReset);
        addMedic = (Button) v.findViewById(R.id.buttonAddMedicament);

        medicBDD = new MedicBDD(v.getContext());

        et1 = (EditText) v.findViewById(R.id.fragment_add_editText_nommedic);
        et2 = (EditText) v.findViewById(R.id.fragment_add_editText_prix);
        et3 = (EditText) v.findViewById(R.id.fragment_add_editText_traitement);
        et4 = (EditText) v.findViewById(R.id.fragment_add_editText_effets);
        et5 = (EditText) v.findViewById(R.id.fragment_add_editText_contreIndic);
        et6 = (EditText) v.findViewById(R.id.fragment_add_editText_quiPeut);
        et7 = (EditText) v.findViewById(R.id.fragment_add_editText_mode);
        et8 = (EditText) v.findViewById(R.id.fragment_add_editText_nb);

        //Listener du bouton pour reset le formulaire d'ajout de médicament
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et1.setText("");
                et2.setText("");
                et3.setText("");
                et4.setText("");
                et5.setText("");
                et6.setText("");
                et7.setText("");
                et8.setText("");
                et1.requestFocus();
            }
        });

        //Listener du bouton pour ajouter le formulaire d'ajout de médicament
        addMedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!et1.getText().toString().equals("") && !et2.getText().toString().equals("") && !et3.getText().toString().equals("") && !et6.getText().toString().equals("") && !et7.getText().toString().equals("") && !et8.getText().toString().equals("")) {
                    if(!medicBDD.CheckIsDataAlreadyInDBorNot(medicBDD.get_TableMedic(),medicBDD.get_COLNOMMEDIC(),et1.getText().toString())) {
                        medicBDD.open();
                        medicBDD.insertMedic(new Medic(et1.getText().toString(), Float.parseFloat(et2.getText().toString()), et3.getText().toString(), et4.getText().toString(), et5.getText().toString(), et6.getText().toString(), et7.getText().toString(), et8.getText().toString()));
                        medicBDD.close();
                        Fragment_SetAlarm fragment = new Fragment_SetAlarm();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                        fragmentTransaction.replace(R.id.fragment_container, fragment);
                        fragmentTransaction.commit();
                    }else{
                        Toast.makeText(v.getContext(), "Ce médicament existe déjà :( \nVous pouvez le consulter dans 'Mes  médicaments'", Toast.LENGTH_LONG).show();
                    }

                }else
                    Toast.makeText(v.getContext(), "Seuls les effets secondaires et le champ 'Ne pas prendre avec' ne sont pas indispensables.", Toast.LENGTH_LONG).show();
            }
        });


        return v;
    }

}
