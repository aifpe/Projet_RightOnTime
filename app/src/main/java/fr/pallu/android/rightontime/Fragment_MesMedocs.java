package fr.pallu.android.rightontime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class Fragment_MesMedocs extends Fragment {
    MedicBDD medicBDD;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mesmedocs_layout,container,false);
        medicBDD = new MedicBDD(v.getContext());
        listView = (ListView) v.findViewById(R.id.listview);
        medicBDD.open();

        List<Medic> values = medicBDD.queueAll();

        ArrayAdapter<Medic> adapter = new ArrayAdapter<Medic>(v.getContext(),
                android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);
        medicBDD.close();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment_Consult fragment = new Fragment_Consult();
                Bundle idBundle = new Bundle();
                idBundle.putString("idConsult", listView.getItemAtPosition(position).toString());
                fragment.setArguments(idBundle);


                android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }
}
