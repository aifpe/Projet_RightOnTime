package fr.pallu.android.rightontime;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseSQLite extends SQLiteOpenHelper{

    public static final String TABLE_MEDIC = "table_medic";
    public static final String COL_ID = "ID";
    public static final String COL_NOMMEDIC = "NOMMEDIC";
    public static final String COL_TRAITEMENT = "TRAITEMENT";
    public static final String COL_EFFETSSECONDAIRES = "EFFETSSECONDAIRES";
    public static final String COL_CONTREINDICATION = "CONTREINDICATION";
    public static final String COL_INDICATIONUTILISATION = "INDICATIONUTILISATION";
    public static final String COL_MODEADMINISTRATION = "MODEADMINISTRATION";
    public static final String COL_NBPILULEAPRENDRE = "NBPILULEAPRENDRE";
    public static final String COL_PRIX = "PRIX";

    private static final String CREATE_BDD = "CREATE TABLE "+ TABLE_MEDIC+" ("+COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_NOMMEDIC+" TEXT NOT NULL, "+COL_TRAITEMENT+" TEXT NOT NULL, "+COL_EFFETSSECONDAIRES+" TEXT NOT NULL, "+COL_CONTREINDICATION+" TEXT NOT NULL, "+COL_INDICATIONUTILISATION+" TEXT NOT NULL, "+COL_MODEADMINISTRATION+" TEXT NOT NULL, "+COL_NBPILULEAPRENDRE+", "+COL_PRIX+");";

    public MaBaseSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE "+TABLE_MEDIC+";");
        onCreate(db);
    }

    public String getTableMedic(){
        return "table_medic";
    }

}
